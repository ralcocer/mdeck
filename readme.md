# MDeck - Markdown Slide Deck Renderer

A presentation app for viewing Appcelerator training modules.

![](http://sht.tl/cjv6FF)


## Install (user mode)

Download MarkdownDeck from - (`link to be provided`)

> MardownDeck only works on MacOS and requires **Git** to be installed


## Install (development mode)

```bash
git clone --recursive git@bitbucket.org:ralcocer/mdeck.git

npm update && git submodule update --remote

```

## Update

```bash
npm update && git submodule update --remote
```

## Packaging

```
tntbuild package.json --clean
```
This builds a native app in the `build` folder.

**Flags**

- `--no-osx-build` Do not compile an OS X version of the application
- `--no-windows-build` Do not compile a windows version of the application.

**Preparing for distribution**

To bundle your app for distribution, you should use the [Apple Package Maker](http:/sht.tl/oVcyFP).

**Cleaning Up**

Prior to distribution, you will need to clean up the package by removing unnecessary files:

- `gulp.js`

# Writing Content

## Presentation Controls

Commands:

- `←` Left arrow - Navigate left one slide
- `→` Right arrow - Navigate right one slide
- `⌘ + ↩` Command + return - Enter / exit fullscreen mode
- `esc` Escape - Exit presentation mode


### Deck Configuration

You can enable capabilities and configuration for a markdown file.  These properties must be defined before any section `---` declaration.

- `theme`: `String` - Set the theme name for custom rendering purposes
- `slidenumbers`: `Boolean` - Hide/show slide numbers. Custom themes can override this. Options: `true`|`false`
- `timing`: `Integer` - Set any value to automatically advance to the next slide after given time. 
- `footer`: `String` - Set a string visible on each slide footer. Custom themes can override this.
- `error`: `String` - Set a fail html string that will be displayed as the `innerText` value of the `rendererror` element.
- `tableclass`: `String` - Set the class name for a table. Example: `striped`
- `format`: `String` - Set either `widescreen` (1920 x 1080) or `standard` (1024 x 768).  Default is `standard`.  It's possible to set a custom format by setting a custom property that specific themes will respond to.  The sets a class name to each slide `<section>`.
- `progress`: `Boolean` - Show / hide the progress bar. Options: `true`|`false`
- 'build-list': `Boolean` - Allows you to step through list bullets one by one to keep your audience focused on what you’re currently talking about.
- 'transition': `String` - Change the way slides transition.  Options: `fade` or none. Defaults to none.
- 'shadows': `Boolean` - Add drop shadows to images. Options: `true`|`false`. Defaults to `false`.

# Local `decks` folder

	ASF
		|- package.json
		|- assets
		|	|- image1.png
		|	`- image2.png
		|- en
		|	|- package.json
		|	|- deck.md
		|	|- LICENSE
		|	`- README.md
		`- es
		|	|- package.json
		|	|- deck.md
		|	|- LICENSE
		`	`- README.md


### Creating Slides

> Important: Slides are stylized by the [Appcelerator Training theme](github.com/ponyshow/theme-appcelerator-training) and controlled by the [`web-client`](github.com/ponyshow/web-client).  The rendering engine is powered by [`pony-render`](https://github.com/Ponyshow/pony-render).  Refer to these projects for the latest markdown syntax options.

A new slide is defined by three dashes `---`, typed on a single line, with an empty line above and below.

Slides are rendered as `<section>` HTML nodes.

You can set a `class` or `id` property as part of the triple dash.  Classes do NOT have a dot notation and are included by default.  IDs require a hash symbol.

> Note: It's preferred to use classes for styling since you can have multiple classes defined.  IDs must be unique and are to URL navigation schemes.  Only use IDs if you are sure it will be uniquely referenced.

**Examples**

```

---cover

---section

---code

---

---#notes

// this is custom ID that will affect navigation.  Must be unique.

```

This will produce three slides.

### Auto-layouts

Images can be `left`, `right` or `fit` the screen.  Left or right positioning will create a two-column layout where an image is positioned and text will flow on either side.

List items can be broken out into two column layout if you define two separated lists.  Example (note the newline separation):

```
- Item 1
- Item 2
- Item 3


- Item 4
- Item 5
- Item 6
```

## Dependencies

### NPM Modules

- Renderer: [pony-render](https://github.com/Ponyshow/pony-render)

**To update:**

Run `npm update`

### Submodules

These are private submodules used in mdeck.

- Client: [web client](https://github.com/Ponyshow/web-client)
- Theme: [appcelerator-training](https://github.com/Ponyshow/theme-appcelerator-training)

**To update:**

Run `git submodule update --remote` to update dependencies.


## Debugging

You can pass a command line argument that is mapped to the `/learning-paths/learning-modules` directory. The argument should match an existing directory path.  The corresponding `deck.md` file will be used for `en` locale.

**Example**

Run `tint main.js appc.PLAT101`

## Learning Paths and badges

The following folder structure needs to be created for each learning path, and will be hosted somewhere on Github.


	ASF
		|- badges
			|- appc.PLAT101
				|- instructor
					|- notes.md
				|- packgage.json
			|- appc.PLAT102
			|- appc.PLAT103
		|- package.json
		|- readme.md


### Learning Path ```package.json```

```javascript
{
  "id": "appcelerator.lp.asf",
  "name": "SDK Fundamentals",
  "slug": "APPC-LP-ASF",
  "description": "SDK Fundamentals Learning Path",
  "version": "1.0.0",
  "copyright": "Appcelerator, Inc.",
  "author": "Ricardo Alcocer",
  "license": "MIT",
  "main": "modules/SDK-000",
  "repository": {
    "type": "git",
    "url": "http://github.com/tzmartin/lp-sdk-fundamentals"
  },
  "bugs": {
    "url": "https://github.com/tzmartin/lp-sdk-fundamentals/issues"
  },
  "keywords": [
    "Appcelerator",
    "SDK",
    "badge",
    "module"
  ],
  "badges": [
    {
      "name": "appc.PLAT101",
      "version": "^0.0.1",
      "url" : "http://github.com/appcelerator-training/learning-modules/appc.PLAT101"
    },
    {
      "name": "appc.PLAT102",
      "version": "^0.0.1",
      "url" : "http://github.com/appcelerator-training/learning-modules/appc.PLAT102"
    },
    {
      "name": "appc.PLAT103",
      "version": "^0.0.1",
      "url" : "http://github.com/appcelerator-training/learning-modules/appc.PLAT103"
    },
    {
      "name": "appc.PLAT104",
      "version": "^0.0.1",
      "url" : "http://github.com/appcelerator-training/learning-modules/appc.PLAT104"
    }
  ]
}
```

### Badge ```package.json```

```javascript
{
  "name": "Appcelerator Development Overview",
  "description": "A high-level view of Alloy",
  "url": "http://github.com/appcelerator/badge-PLAT101",
  "copyright": "2015 Appcelerator, Inc.",
  "author": "Ricardo Alcocer",
  "badgeID": "PLAT101",
  "version": "1.0.0",
  "namespace": "appc.badge.PLAT101",
  "icon": "badge.icon.png",
  "license": "Commercial",
  "prerequisites": [
    {
      "badgeid": "JS100",
      "version": "^1.0.0"
    },
    {
      "badgeid": "STUD001",
      "version": "^1.0.0"
    },
    {
      "badgeid": "SDK102",
      "version": "^1.0.0"
    }
  ],
  "personas": [
    "1",
    "2",
    "3",
    "4"
  ],
  "difficulty": [
    "1"
  ],
  "supplements": {
    "code": [
      {
        "type": "lab",
        "url": "http://github.com/appcelerator/badge_alloy100-lab"
      },
      {
        "type": "tryit",
        "url": "http://"
      }
    ],
    "slides": {
      "en": [
        {
          "type": "slides",
          "url": "http://drive.google.com/somelonghash"
        },
        {
          "type": "pdf",
          "url": "http://file.pdf"
        }
      ],
      "es": [
        {
          "type": "slides",
          "url": "http://drive.google.com/somelonghash"
        },
        {
          "type": "pdf",
          "url": "http://file.pdf"
        }
      ]
    }
  },
  "assessments": [
    {
      "type": "poll",
      "url": "http://github.com/appcelerator/badge_PLAT101-poll"
    },
    {
      "type": "quiz",
      "url": "http://"
    },
    {
      "type": "exam",
      "url": "http://github.com/appcelerator/badge_PLAT101-exam"
    }
Add a comment to this line
  ]
}
```

# Todo

- Look at [https://bitbucket.org/ralcocer/mdeck/issues?status=new&status=open](https://bitbucket.org/ralcocer/mdeck/issues?status=new&status=open)