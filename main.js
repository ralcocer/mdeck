JSON = require('lib_json');

var appConfig 	= require('mod_config').getConfig();
appConfig.isMac = require('os').platform().toLowerCase() == "darwin";

var MW 			= require('mod_mainwin');
var MainWin 	= new MW.MainWin(appConfig);


// strip the node executable from the args
var args = process.argv.slice();
if (args[0].replace(/\\/g, '/').split('/').pop().replace(/\.exe$/, '') == process.execPath.replace(/\\/g, '/').split('/').pop().replace(/\.exe$/, '')) {
	args.shift();
}
	
args.shift();

MainWin.load(args);