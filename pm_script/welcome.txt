•	Select "Default" to show the default welcome:
	You will be guided through the steps necessary to install this software.
•	Select "Embedded" to enter a text to be embedded in the distribution.
•	Or specify your custom welcome file by dropping it here.