# MDeck - Markdown Presentation Engine

MDeck is a Mac desktop application designed to automate the process of obtaining up-to-date Appcelerator-certified training content, while standardizing the way content is delivered to training partners and trainees.

MDeck slides are written in Markdown, making them easier to write, maintain and collaborate on.  All content updates and versioning is managed by Github and the main repository is located at [http://github.com/appcelerator-training/learning-paths](http://github.com/appcelerator-training/learning-paths).

## Installation and Configuration

To install MDeck simply double-click the **pkg** file and follow the steps of the installer.  Once it's installed, look for it in you /Applications folder and launch it.  When you launch MDeck for the first time, the first thing you want to do is synchronize the content files by clicking on the **Sync** button.

![](assets/firstrun.png)

This process will download the latest version of all the available Appcelerator-approved Learning Paths and Modules.  All the content will be stored in a folder structure MDeck will create in your home folder.  You should hit the Sync button every once in a while to ensure you're always using the latest version of the content.

![](assets/local.png)

The **yourinstructor.md** and **housekeeping.md** files are template files that are used as overrides for the ones that ship with MDeck.  You should open them and make the necessary changes so they include your name, photo and pertinent information.


## Using MDeck

When MDeck is launched, it will show the main screen, which will give you access to MDeck's main features.

![](assets/mainwin.png) 

Follow these steps to start working with a particular slide deck:

![](assets/step1.png)
![](assets/step2.png)
![](assets/step3.png)
![](assets/step4.png)

Once your slide is shown on the main screen, you're ready to start delivering the presentation.  At this point, MDeck works just as any other presentation system like *Powerpoint* or *Keynote*.  Below you'll find a list of available keyboard commands.

> MDeck also supports traversing slides using a remote control

![](assets/kb_control.png)

---
Rev. August 2015
Copyright Appcelerator, Inc.

